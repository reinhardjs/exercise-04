package com.btpn.chip;

public interface Notifiable {
    public void notifyParkingSpaceFull(ParkingLot parkinglot);

    public void notifyParkingSpaceAvailable(ParkingLot parkingLot);
}
