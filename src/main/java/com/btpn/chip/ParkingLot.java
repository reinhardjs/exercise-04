package com.btpn.chip;

import java.util.ArrayList;
import java.util.List;

public class ParkingLot {
    private List<ParkAble> parkedCar = new ArrayList<>();
    private int maximumSpot;
    private List<Notifiable> notifiables = new ArrayList<>();

    ParkingLot(int maximumSpot) {
        this.maximumSpot = maximumSpot;
    }

    ParkingLot(int maximumSpot, List<Notifiable> notifiables) {
        this.maximumSpot = maximumSpot;
        this.notifiables.addAll(notifiables);
    }

    public void park(ParkAble car) {
        if (isParkedCarFull()) {
            throw new ParkingSpotNotAvailableException();
        }
        if (isCarAlreadyParked(car)) {
            throw new CarIsAlreadyParkedException();
        }
        this.parkedCar.add(car);
        if (isParkedCarFull()) {
            notifyFParkingSpaceFull();
        }
    }

    public void addNotifiable(Notifiable notifiable) {
        if (notifiables.contains(notifiable)) {
            throw new NotifiablePersonIsAlreadyExistException();
        }
        this.notifiables.add(notifiable);
        if (isParkedCarFull()) {
            notifyFParkingSpaceFull();
            return;
        }
        if (isParkedCarComeAvailable()) {
            notifyParkingSpaceAvailable();
            return;
        }
    }

    private void notifyFParkingSpaceFull() {
        for (Notifiable notifiable : notifiables) {
            notifiable.notifyParkingSpaceFull(this);
        }
    }

    private void notifyParkingSpaceAvailable() {
        for (Notifiable notifiable : notifiables) {
            notifiable.notifyParkingSpaceAvailable(this);
        }
    }

    public void unPark(ParkAble car) {
        if (!isCarAlreadyParked(car)) {
            throw new CarIsNotParkedException();
        }
        parkedCar.remove(car);
        if (isParkedCarComeAvailable()) {
            notifyParkingSpaceAvailable();
        }
    }

    private boolean isParkedCarComeAvailable() {
        return parkedCar.size() == this.maximumSpot - 1;
    }

    public boolean isParkedCarFull() {
        return parkedCar.size() == this.maximumSpot;
    }

    public boolean isCarAlreadyParked(ParkAble car) {
        return parkedCar.contains(car);
    }

    public int getMaximumSpot() {
        return this.maximumSpot;
    }
}