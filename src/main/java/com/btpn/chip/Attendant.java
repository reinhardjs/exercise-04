package com.btpn.chip;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Attendant implements Notifiable {
    private Set<ParkingLot> parkingLots = new HashSet<>();
    private List<ParkingLot> parkingLotAvailable = new ArrayList<>();

    Attendant(Set<ParkingLot> parkingLot) {
        this.parkingLots.addAll(parkingLot);
        this.subscribeToParkingLot();
    }

    private void subscribeToParkingLot() {
        for (ParkingLot parkingLot : parkingLots) {
            parkingLot.addNotifiable(this);
        }
    }

    public void park(ParkAble car) {
        if (isParkingLotsAvailable()) {
            List<ParkingLot> sortedParkingLot = parkingLotAvailable.stream()
                    .sorted(Comparator.comparingInt(ParkingLot::getMaximumSpot)).collect(Collectors.toList());
            Collections.reverse(sortedParkingLot);
            sortedParkingLot.get(0).park(car);
            return;
        }
        throw new ParkingSpotNotAvailableException();
    }

    public void unPark(ParkAble car) {
        for (ParkingLot parkingLot : parkingLots) {
            if (parkingLot.isCarAlreadyParked(car)) {
                parkingLot.unPark(car);
                return;
            }
        }
        throw new ParkingSpotNotAvailableException();
    }

    public ParkingLot findMax() {
        ParkingLot max = null;
        int maxSize = 0;
        int size = 0;
        for (ParkingLot parkingLot : parkingLots) {
            size = parkingLot.getMaximumSpot();
            if (size >= maxSize) {
                max = parkingLot;
            }
        }
        return max;
    }

    @Override
    public void notifyParkingSpaceFull(ParkingLot parkingLot) {
        parkingLotAvailable.remove(parkingLot);
    }

    @Override
    public void notifyParkingSpaceAvailable(ParkingLot parkingLot) {
        parkingLotAvailable.add(parkingLot);
    }

    private boolean isParkingLotsAvailable() {
        return !this.parkingLotAvailable.isEmpty();
    }
}
