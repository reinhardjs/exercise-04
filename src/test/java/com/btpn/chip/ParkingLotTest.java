package com.btpn.chip;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class ParkingLotTest {
    @Test
    void park_shouldNotThrowParkingSpotNotAvailableException_whenParkingSpotIsNotOccupied() {
        ParkAble crv = new ParkAble() {
        };
        ParkingLot parkingLot = new ParkingLot(1);

        Assertions.assertDoesNotThrow(() -> parkingLot.park(crv));
    }

    @Test
    void unPark_shouldThrowCarIsNotParkedException_whenThereIsNoParkingCar() {
        ParkAble avanza = new ParkAble() {
        };
        ParkingLot parkingLot = new ParkingLot(1);

        Assertions.assertThrows(CarIsNotParkedException.class, () -> parkingLot.unPark(avanza));
    }

    @Test
    void park_shouldThrowCarIsAlreadyParked_whenThereIsParkingCar() {
        ParkAble avanza = new ParkAble() {
        };
        ParkAble brv = new ParkAble() {
        };
        ParkingLot parkingLot = new ParkingLot(4);
        parkingLot.park(avanza);
        parkingLot.park(brv);

        Assertions.assertThrows(CarIsAlreadyParkedException.class, () -> parkingLot.park(avanza));
    }

    @Test
    void unPark_shouldNotThrowCarIsNotParkedException_whenThereIsAvanza() {
        ParkAble Avanza = new ParkAble() {
        };
        ParkingLot parkingLot = new ParkingLot(1);
        parkingLot.park(Avanza);

        Assertions.assertDoesNotThrow(() -> parkingLot.unPark(Avanza));

    }

    @Test
    void park_shouldNotThrowAfterSuccessfullyRemoveAvanza_whenAvanzaParkedAgain() {
        ParkAble avanza = new ParkAble() {
        };
        ParkingLot parkingLot = new ParkingLot(3);
        parkingLot.park(avanza);
        parkingLot.unPark(avanza);

        Assertions.assertDoesNotThrow(() -> parkingLot.park(avanza));
    }

    @Test
    void park_shouldNotThrowParkingSpotNotAvailableException_whenParkingSpotIsMoreThanOneCar() {
        ParkAble crv = new ParkAble() {
        };
        ParkAble brv = new ParkAble() {
        };
        ParkingLot parkingLot = new ParkingLot(3);
        parkingLot.park(brv);

        Assertions.assertDoesNotThrow(() -> parkingLot.park(crv));
    }

    @Test
    void park_shouldNotifiedNotifiable_whenParkingSpaceIsFull() {
        ParkAble avanza = new ParkAble() {
        };
        ParkAble brio = new ParkAble() {
        };
        ParkingLot parkingLot = new ParkingLot(2);
        Notifiable owner = Mockito.mock(Notifiable.class);
        Notifiable trafficCop = Mockito.mock(Notifiable.class);
        parkingLot.addNotifiable(owner);
        parkingLot.addNotifiable(trafficCop);
        parkingLot.park(avanza);
        parkingLot.park(brio);

        Mockito.verify(owner).notifyParkingSpaceFull(parkingLot);
        Mockito.verify(trafficCop).notifyParkingSpaceFull(parkingLot);
    }

    @Test
    void unPark_shouldNotifiedNotifiable_whenParkingSpaceIsComeAvailable() {
        ParkAble avanza = new ParkAble() {
        };
        ParkAble brio = new ParkAble() {
        };
        ParkingLot parkingLot = new ParkingLot(2);
        Notifiable owner = Mockito.mock(Notifiable.class);
        Notifiable trafficCop = Mockito.mock(Notifiable.class);
        parkingLot.addNotifiable(owner);
        parkingLot.addNotifiable(trafficCop);
        parkingLot.park(avanza);
        parkingLot.park(brio);
        parkingLot.unPark(brio);

        Mockito.verify(owner).notifyParkingSpaceAvailable(parkingLot);
        Mockito.verify(trafficCop).notifyParkingSpaceAvailable(parkingLot);
    }

    @Test
    void addNotifiable_shouldThrowNotifiablePersonExistException_whenParkingSpaceIsFull() {
        ParkingLot parkingLot = new ParkingLot(2);
        Notifiable owner = Mockito.mock(Notifiable.class);
        Notifiable trafficCop = Mockito.mock(Notifiable.class);
        parkingLot.addNotifiable(owner);
        parkingLot.addNotifiable(trafficCop);

        Assertions.assertThrows(NotifiablePersonIsAlreadyExistException.class, () -> parkingLot.addNotifiable(owner));
    }
}