package com.btpn.chip;

import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class AttendanceTest {
    @Test
    void park_shouldCallParkFromParkingLot_whenAttendanceParkCar() {
        ParkAble mitsubishi = new ParkAble() {
        };
        ParkingLot realParkingLot = new ParkingLot(1);
        ParkingLot parkingLot = Mockito.spy(realParkingLot);
        Set<ParkingLot> parkingLots = new HashSet<>();
        parkingLots.add(parkingLot);
        Attendant attendant = new Attendant(parkingLots);
        attendant.notifyParkingSpaceAvailable(parkingLot);

        Assertions.assertDoesNotThrow(() -> attendant.park(mitsubishi));
        Mockito.verify(parkingLot).park(mitsubishi);
    }

    @Test
    void unpark_shouldCallUnparkFromParkingLot_whenAttendanceUnparkCar() {
        ParkAble mitsubishi = new ParkAble() {
        };
        ParkingLot realParkingLot = new ParkingLot(1);
        ParkingLot parkingLot = Mockito.spy(realParkingLot);
        Set<ParkingLot> parkingLots = new HashSet<>();
        parkingLots.add(parkingLot);
        Attendant attendance = new Attendant(parkingLots);

        parkingLot.park(mitsubishi);
        attendance.unPark(mitsubishi);

        Mockito.verify(parkingLot).unPark(mitsubishi);
    }
    @Test
    void findMax_shouldReturnParkingSpot2WithMaximumSlotSize_whenTheInputIsParkinLot1SizeIs2AndParkingLot2SizeIs3() {

        ParkingLot realParkingLot = new ParkingLot(2);
        ParkingLot menaraParkingLot = new ParkingLot(3);
        Set<ParkingLot> parkingLots = new HashSet<>();
        parkingLots.add(realParkingLot);
        parkingLots.add(realParkingLot);
        Attendant attendance = new Attendant(parkingLots);

        Assertions.assertEquals(menaraParkingLot, attendance.findMax());
    }
}